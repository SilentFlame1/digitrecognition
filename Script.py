import tensorflow as tf
from os import path, getcwd, chdir

# To stop processing when the accuracy passes the target mark, also saves computational time.
class myCallback(tf.keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs={}):
    if(logs.get('acc')>0.99):
      print("\nReached 99% accuracy so cancelling training!")
      self.model.stop_training = True

callbacks = myCallback()

def train_mnist():
    mnist = tf.keras.datasets.mnist

    (x_train, y_train),(x_test, y_test) = mnist.load_data(path=path)
    
    #changing the grid values to 0 and 1 (Normalization).
    x_train  = x_train / 255.0
    x_test = x_test / 255.0
    
    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(1024, activation=tf.nn.relu),
        tf.keras.layers.Dense(512, activation=tf.nn.relu),
        tf.keras.layers.Dense(10,activation=tf.nn.softmax)
    ])
    print(model.summary)
    
#     1. Defining a sequence of layers for Neural Networks.
#     2. Flatening the 2D images into 1D sequence.
#     3. Adding 2 layers of neurons, along with an activation function. (RELU is =x when x>= else 0)
#     4. Output layer with size same as number of clasees. (Softmax effectively extracts the biggest one from the set).

    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    
    # model fitting
    history = model.fit(
                x_train, y_train, epochs=10, callbacks=[callbacks]
    )
    # model fitting
    return history.epoch, history.history['acc'][-1]
    
train_mnist()
# Output: ([0, 1, 2, 3, 4, 5], 0.991)