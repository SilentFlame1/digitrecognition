# DigitRecognition

Writing an MNIST classifier that trains to 99% accuracy or above, and does it without a fixed number of epochs -- i.e. it stops training once we reach that level of accuracy.